-- upgrade --
CREATE TABLE IF NOT EXISTS "chatmodel"
(
    "id"                  BIGSERIAL    NOT NULL PRIMARY KEY,
    "type"                VARCHAR(10)  NOT NULL,
    "username"            VARCHAR(32) UNIQUE,
    "first_name_or_title" VARCHAR(255) NOT NULL,
    "last_name"           VARCHAR(64),
    "created_at"          TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"          TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_chatmodel_usernam_b1d609" ON "chatmodel" ("username");
COMMENT ON COLUMN "chatmodel"."type" IS 'private: private\ngroup: group\nsupergroup: supergroup\nchannel: channel';
CREATE TABLE IF NOT EXISTS "disablechannelsmodel"
(
    "id"      SERIAL NOT NULL PRIMARY KEY,
    "chat_id" BIGINT NOT NULL UNIQUE REFERENCES "chatmodel" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "filtermodel"
(
    "id"           SERIAL      NOT NULL PRIMARY KEY,
    "comment"      VARCHAR(32) NOT NULL,
    "handler_type" VARCHAR(10) NOT NULL,
    "handler"      VARCHAR(128),
    "action"       VARCHAR(12) NOT NULL,
    "chat_id"      BIGINT      NOT NULL UNIQUE REFERENCES "chatmodel" ("id") ON DELETE CASCADE
);
COMMENT ON COLUMN "filtermodel"."handler_type" IS 'text: text\ntext_regex: text_regex\ndocument: document\nphoto: photo\naudio: audio\nvideo: video\nvoice: voice\nvideo_note: video_note\nsticker: sticker\ncontact: contact\nlocation: location';
COMMENT ON COLUMN "filtermodel"."action" IS 'delete: delete\nsend_message: send_message';
CREATE TABLE IF NOT EXISTS "aerich"
(
    "id"      SERIAL       NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app"     VARCHAR(100) NOT NULL,
    "content" JSONB        NOT NULL
);
