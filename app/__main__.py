import asyncio

from loguru import logger

from app.modules import load_modules, pre_start_modules
from app.services.bot import dp, bot
from app.services.db import start_db

logger.info("Blue bot")

dp.include_router(load_modules())


async def pre_start():
    await start_db()
    await pre_start_modules()


# Start bot
loop = asyncio.new_event_loop()
loop.run_until_complete(pre_start())

logger.info("Started polling!")
dp.run_polling(bot)
