# Function to load modules
import importlib
import os
from dataclasses import dataclass
from types import ModuleType
from typing import List

from aiogram import Router
from loguru import logger

from app.config import SETTINGS


@dataclass
class LoadedModule:
    name: str
    module: ModuleType
    router: Router


# Tuple of all found modules
ALL_MODULE_NAMES = tuple(x for x in os.listdir(os.path.dirname(__file__)) if not x.startswith("_"))
LOADED_MODULES: List[LoadedModule] = []


def load_modules() -> Router:
    logger.info("Loading modules...")

    module_root_router = Router(name="module_root")

    all_modules = SETTINGS.to_load if type(SETTINGS.to_load) is list else ALL_MODULE_NAMES
    # Remove disabled modules from list
    to_load = [x for x in all_modules if x not in (SETTINGS.to_not_load or [])]

    for module_name in to_load:
        logger.debug(f"Loading module {module_name}:")
        module = importlib.import_module(f"app.modules.{module_name}")

        if hasattr(module, "__pre_load__"):
            logger.debug(f"- Running pre-load function for module {module_name}")
            module.__pre_load__()
        if hasattr(module, "router"):
            logger.debug(f"- Adding router for module {module_name}")
            router = module.router
            module_root_router.include_router(router)
        else:
            logger.warning(f"- No router found for module {module_name}")
            router = None

        LOADED_MODULES.append(LoadedModule(
            name=module_name,
            module=module,
            router=router
        ))
        logger.opt(colors=True).debug(f"<green>Module {module_name} loaded.</>")

    # All modules loaded
    logger.debug("Running post-load functions:")
    for loaded_module in LOADED_MODULES:
        if hasattr(loaded_module.module, "__post_load__"):
            logger.debug(f"- Running post-load function for module {loaded_module.name}")
            loaded_module.module.__post_load__()
    logger.opt(colors=True).debug("<green>Post-load modules finished!</>")
    logger.info(f"All modules loaded: {', '.join(x.name for x in LOADED_MODULES)}")

    return module_root_router


async def pre_start_modules():
    # Function that calls pre-start function in modules before starting the bot
    # This is useful for modules that need to do some async stuff before the bot starts
    logger.info("Running pre-start functions:")
    for loaded_module in LOADED_MODULES:
        if hasattr(loaded_module.module, "__pre_start__"):
            logger.debug(f"- Running pre-start function for module {loaded_module.name}")
            await loaded_module.module.__pre_start__()
    logger.opt(colors=True).debug("<green>Pre-start modules finished!</>")
