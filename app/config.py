from typing import Optional, List

from pydantic import BaseSettings


class Settings(BaseSettings):
    token: str

    db_url: str = "postgres://postgres:123@localhost:5432/postgres"

    # Migrates database to latest version automatically
    migrator: bool = True

    # Generates and updates the database schema on each run (Please don't use in production)
    generate_schemas: bool = False

    to_load: Optional[List[str]] = None
    to_not_load: Optional[List[str]] = None

    redis_on: bool = False
    redis_host: str = "localhost"
    redis_port: int = 6379
    redis_password: Optional[str] = None
    redis_cache_db: int = 0
    redis_fsm_db: int = 1
    redis_client_name: str = "blue"

    class Config:
        env_file = "data/config.env"


SETTINGS = Settings()  # type: ignore

DB_CONFIG = {
    "connections": {"default": SETTINGS.db_url},
    "apps": {
        "default": {"models": ["app.models", "aerich.models"]}
    },
    "default_connection": "default",
}
