from loguru import logger
from tortoise import Tortoise

from app.config import SETTINGS, DB_CONFIG
from app.utils.migrator import migrator


async def start_db():
    logger.info("Starting database...")
    await Tortoise.init(config=DB_CONFIG)

    if SETTINGS.generate_schemas:
        logger.info("Generating database schemas...")
        await Tortoise.generate_schemas()

    await migrator()


async def stop_db():
    await Tortoise.close_connections()
