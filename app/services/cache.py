from app.config import SETTINGS


def get_cache():
    # Function to return cache based on settings
    if SETTINGS.redis_on:
        from aiocache import RedisCache
        return RedisCache(
            endpoint=SETTINGS.redis_host,
            port=SETTINGS.redis_port,
            password=SETTINGS.redis_password,
            db=SETTINGS.redis_cache_db
        )
    else:
        from aiocache import SimpleMemoryCache
        return SimpleMemoryCache()


cache = get_cache()
