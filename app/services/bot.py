from aiogram import Bot, Dispatcher

from app.config import SETTINGS


# Function to get storage based on settings
def get_storage():
    if SETTINGS.redis_on:
        from aioredis import Redis
        from aiogram.dispatcher.fsm.storage.redis import RedisStorage

        redis = Redis(
            host=SETTINGS.redis_host,
            port=SETTINGS.redis_port,
            password=SETTINGS.redis_password,
            db=SETTINGS.redis_fsm_db,
        )
        return RedisStorage(redis)
    else:
        from aiogram.dispatcher.fsm.storage.memory import MemoryStorage
        return MemoryStorage()


bot = Bot(token=SETTINGS.token, parse_mode="HTML")
dp = Dispatcher(get_storage())
