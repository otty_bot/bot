import html
from enum import Enum

from tortoise import fields
from tortoise.models import Model

from .disablechannels import DisableChannelsModel
from .filters import FilterModel


class ChatType(Enum):
    private = "private"
    group = "group"
    supergroup = "supergroup"
    channel = "channel"


class ChatModel(Model):
    id = fields.BigIntField(pk=True)
    type = fields.CharEnumField(ChatType, max_length=10)
    username = fields.CharField(max_length=32, null=True, unique=True, index=True)
    first_name_or_title = fields.CharField(255)
    last_name = fields.CharField(64, null=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    disable_channels: fields.OneToOneRelation[DisableChannelsModel]
    FilterModel: fields.OneToOneRelation[FilterModel]

    @property
    def html_first_or_title(self) -> str:
        return html.escape(self.first_name_or_title)

    @property
    def html_last_name(self) -> str:
        return html.escape(self.last_name)

    @property
    def html_full_name(self) -> str:
        if self.last_name:
            return f"{self.html_first_or_title} {self.html_last_name}"
        return self.html_first_or_title
