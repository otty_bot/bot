from tortoise import fields
from tortoise.models import Model


class DisableChannelsModel(Model):
    chat = fields.OneToOneField("default.ChatModel", related_name="disable_channels")
