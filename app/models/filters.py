from enum import Enum

from tortoise import fields
from tortoise.models import Model


class FilterHandlerType(Enum):
    text = "text"
    text_regex = "text_regex"
    document = "document"
    photo = "photo"
    audio = "audio"
    video = "video"
    voice = "voice"
    video_note = "video_note"
    sticker = "sticker"
    contact = "contact"
    location = "location"


class FilterActionType(Enum):
    delete = "delete"
    send_message = "send_message"


class FilterModel(Model):
    chat = fields.OneToOneField("default.ChatModel", related_name="filters")
    comment = fields.CharField(32)
    handler_type = fields.CharEnumField(FilterHandlerType)
    handler = fields.CharField(128, null=True)
    action = fields.CharEnumField(FilterActionType)
