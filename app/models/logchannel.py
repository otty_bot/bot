from tortoise import fields
from tortoise.models import Model


class LogChannel(Model):
    chat = fields.OneToOneField("default.Chat", related_name="log_channel", pk=True)
    channel = fields.OneToOneField("default.Chat")
