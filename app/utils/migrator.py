from time import sleep

from aerich import Command
from loguru import logger
from tortoise.exceptions import OperationalError

from app.config import SETTINGS, DB_CONFIG

command = Command(tortoise_config=DB_CONFIG, app='default', location='./migrations/default')


async def upgrade():
    logger.warning("Sleeping for 20 seconds before applying...")
    sleep(20)
    await command.upgrade()
    logger.info("Migrations applied!")


async def migrator():
    if not SETTINGS.migrator:
        logger.warning("Migrator is disabled!")
        return

    try:
        heads = await command.heads()

        if heads:
            available_migrations = "\n\t".join(heads)
            logger.warning(f'Available migrations: {available_migrations}')
            await upgrade()
        else:
            logger.info("The database schema is up to date!")
    except OperationalError:
        logger.info("The database is looks like blank, applying all migrations...")
        await upgrade()
